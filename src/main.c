//
// Created by Andrey Vsiliev on 29.12.2021.
//

#include "tests.h"

int main(){
    test1_allocate_memory();
    test2_free_block_memory();
    test3_free_2blocks_memory();
    test4_memory_ended_extends_it();
    test5_extends_region_into_some_area();
}
