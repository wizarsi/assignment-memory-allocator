//
// Created by Andrey Vsiliev on 31.12.2021.
//

#include "tests.h"
#include "mem.h"
#include "util.h"
#include "mem_internals.h"


#define HEAP_SIZE 10000

static void map_by_heap(void* heap_addr, uint8_t offset,size_t length);

void test1_allocate_memory() {
    printf("\nTест 1. Выделение памяти\n");
    void *heap_addr = heap_init(HEAP_SIZE); //получаем инициализированого heap

    printf("Куча до аллокации\n");
    debug_heap(stdout, heap_addr);
    int8_t *block_numbers=_malloc(sizeof(int8_t) * 20);
    block_numbers[0]=9;
    block_numbers[1]=52;
    block_numbers[2]=99;

    printf("Куча после 1 аллокации\n");
    debug_heap(stdout, heap_addr);

    _malloc(100);

    printf("Куча после 2 аллокации\n");
    debug_heap(stdout, heap_addr);

    printf("%s", "\n---------------------------------------\n");

}

void test2_free_block_memory() {
    printf("\nTест 2. Освобождение блоков\n");
    void *heap_addr = heap_init(HEAP_SIZE);

    void *block_first = _malloc(100);
    _malloc(200);
    _malloc(300);

    printf("Куча до очищения\n");
    debug_heap(stdout, heap_addr);

    _free(block_first);
    printf("Куча после очищения\n");
    debug_heap(stdout, heap_addr);
    printf("%s", "\n---------------------------------------\n");

}

void test3_free_2blocks_memory() {
    printf("\nTест 3. Освобождение 2 блоков\n");
    void *heap_addr = heap_init(HEAP_SIZE);

    void *block_first = _malloc(200);
    void *block_second = _malloc(300);
    _malloc(400);

    printf("Куча до очищения\n");
    debug_heap(stdout, heap_addr);

    _free(block_second);
    printf("Куча после очищения от 2-го блока\n");
    debug_heap(stdout, heap_addr);

    _free(block_first);
    printf("Куча после очищения от 1-го блока\n");
    debug_heap(stdout, heap_addr);
    printf("%s", "\n---------------------------------------\n");

}

void test4_memory_ended_extends_it() {
    printf("\nTест 4. Память закончилась, расширение старого региона\n");
    void *heap_addr = heap_init(HEAP_SIZE);

    _malloc(1000);
    printf("Куча после аллокации 1-го блока\n");
    debug_heap(stdout, heap_addr);

    _malloc(12000);
    printf("Куча после расширения памяти\n");
    debug_heap(stdout, heap_addr);
    printf("%s", "\n---------------------------------------\n");

}


void test5_extends_region_into_some_area() {
    printf("\nTест 5. Память закончилась, добавление нового региона памяти \n");
    void *heap_addr = heap_init(HEAP_SIZE);
    printf("Куча до расширения памяти\n");
    debug_heap(stdout, heap_addr);

    struct block_header *reg = (struct block_header*)heap_addr;
    while (reg->next!=NULL) reg=reg->next;

    map_by_heap((void *)reg,(uint8_t)13000,30000);

    _malloc(29000);
    printf("Куча после аллокации блока в созданном регионе памяти\n");
    debug_heap(stdout,heap_addr);
    printf("%s", "\n---------------------------------------\n");

}

static void map_by_heap(void* heap_addr, uint8_t offset,size_t length){
    uint8_t * extended_region = (uint8_t *)heap_addr + offset; //адрес с которого мы продолжаем регион
    mmap((void *)extended_region, length, PROT_READ | PROT_WRITE, 0, 0, 0);
}


