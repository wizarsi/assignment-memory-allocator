#include <stdarg.h>

#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size
size_from_capacity( block_capacity
cap );
extern inline block_capacity
capacity_from_size( block_size
sz );

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool

region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    /*  ??? */

    const size_t sz = region_actual_size(query);
    void *address = map_pages(addr, sz, MAP_FIXED);
    if (address==(void *)-1) address = map_pages(addr, sz, 0);

    struct region my_reg;

    my_reg.addr = address;
    my_reg.extends = true;
    my_reg.size = sz;

    block_size size = {.bytes = sz};
    block_init(address, size, NULL);

    return my_reg;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region reg = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&reg)) return NULL;

    return reg.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free && query + offsetof(
    struct block_header, contents ) +BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    /*  ??? */

    if (!block_splittable(block, query)) return false;

    //размер следуещего блока
    block_size start_sz = size_from_capacity(block->capacity);
    block_size block_sz = (block_size) {.bytes=start_sz.bytes - query};

    //создаем следующий блок
    block->capacity.bytes = query;
    void *next_block_addr = block_after(block);
    block_init(next_block_addr, block_sz, block->next);
    block->next = next_block_addr;

    return true;

}


/*  --- Слияние     соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    /*  ??? */
    if (mergeable(block, block->next)) {
        block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
        block->next = block->next->next;
        return true;
    }
    return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    struct block_search_result result;
    struct block_header *buf = block;
    struct block_header *buf_last = block; //храним самый последний элемент списка, так как сразу после него NULL в buf
    while (buf) {
        buf_last = buf;
        if (block_is_big_enough(sz, buf) && buf->is_free) {
            result.block = buf;
            result.type = BSR_FOUND_GOOD_BLOCK;
            return result;
        }
        buf = buf->next;
    }
    result.block = buf_last;
    result.type = BSR_REACHED_END_NOT_FOUND;
    return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result found_res = find_good_or_last(block, query);
    if (!found_res.type) split_if_too_big(found_res.block, query);
    return found_res;
}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    /*  ??? */
    struct region allocated_reg = alloc_region(block_after(last), query);
    last->next = allocated_reg.addr;
    last->next->is_free = false;
    struct block_header* res = last->next;
    if(try_merge_with_next(last)){
        res = last;
    }
    split_if_too_big(res, query);
    return res;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    /*  ??? */
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result found_res = try_memalloc_existing(query, heap_start);
    switch (found_res.type) {
        case BSR_REACHED_END_NOT_FOUND:
            return grow_heap(found_res.block, query);
        case BSR_FOUND_GOOD_BLOCK:
            found_res.block->is_free = false;//taken
            return found_res.block;
        default:
            return NULL;
    }
}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(
    struct block_header, contents));
}

void _free(void *mem) {
    /*  ??? */

    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));
}
